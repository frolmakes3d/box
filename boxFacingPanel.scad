include <boxDefaultParams.scad>
use<boxPanel.scad>

module boxFacingPanel() {
    boxPanel(
    L = boxWidth,
    l = boxHeight,
    thickness = boxThickness,
    fingerJointSide = [0, - 1, - 1, - 1],
    fingerJointSize = boxFingerJointSize
    );
}

module moveBoxFacingPanel(front = 1) {
    translate([0, - 1 * front * boxLength / 2 + front * boxThickness / 2, 0]) {
        rotate(90, [1, 0, 0]) {
            children();
        }
    }
}

module moveBoxFacingPanelSheetCut(front = 1) {
    translate([(front == 1)?0:boxHeight + boxSpaceBetweenCuts, boxLength / 2 + boxWidth / 2 + boxSpaceBetweenCuts, 0])
        rotate(-90, [0, 0, 1])
            children();
}
