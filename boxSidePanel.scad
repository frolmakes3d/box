include <boxDefaultParams.scad>
use<boxPanel.scad>

module boxSidePanel() {
    boxPanel(
    L = boxHeight,
    l = boxLength,
    thickness = boxThickness,
    fingerJointSide = [1, - 1, 1, 0],
    fingerJointSize = boxFingerJointSize
    );
}

module moveBoxSidePanel(left = 1) {
    translate([- 1 * left * boxWidth / 2 + left * boxThickness / 2, 0, 0]) {
        rotate(- 90, [0, 1, 0]) {
            children();
        }
    }
}

module moveBoxSidePanelSheetCut(left = 1) {
    translate([(left == 1)?0:boxHeight + boxSpaceBetweenCuts,0,0])
        children();
}
