module notches(
numberOfNotches = 3,
notchesWidth = 3,
notchesDepth = 10,
sideLength = 100,
thickness = 3,
panelHeight = 50,
isFace = true
) {
    betweenNotchesCenter = sideLength / (numberOfNotches + 1);
    rotate(isFace?0:-90, [0, 0, 1])
        translate([0,panelHeight / 2 - notchesDepth / 2, 0])
            translate([- sideLength / 2, 0, 0])
                for (i = [1: numberOfNotches]) {
                    translate([betweenNotchesCenter * i, 0, 0])
                        cube([notchesWidth, notchesDepth + 1, thickness + 1], true);
                }
}
