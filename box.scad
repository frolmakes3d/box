module box() {
    color("blue") moveBoxBottom() boxBottom();
    color("purple") moveBoxSidePanel(1) boxSidePanel();
    color("red") moveBoxSidePanel(- 1) boxSidePanel();
    color("green") moveBoxFacingPanel(1) boxFacingPanel();
    color("orange") moveBoxFacingPanel(- 1) boxFacingPanel();
}
