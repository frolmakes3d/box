include <boxDefaultParams.scad>
use<boxPanel.scad>
module boxBottom() {
    boxPanel(L = boxWidth,
    l = boxLength,
    thickness = boxThickness,
    fingerJointSide = [1, 1, 1, 1],
    fingerJointSize = boxFingerJointSize
    );
}
module moveBoxBottom() {
    translate([0, 0, - boxHeight / 2 + boxThickness / 2]) {
        children();
    }

}
module moveBoxBottomSheetCut() {
    translate([boxWidth/2 + 1.5*(boxHeight + boxSpaceBetweenCuts),0, 0])
        children();
}
