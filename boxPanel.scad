//finger joint 0:no finger joint, 1:out, -1:in
module boxPanel(
L = 150,
l = 100,
thickness = 3,
fingerJointSide = [0, 0, 0, 0],
fingerJointSize = 20,
) {

    module joint(index = 0) {
        if (fingerJointSide[index] != 0) {
            rotate(index * 90, [0, 0, 1])
                translate([0, ((index % 2 == 1)?L:l) / 2 - thickness]) {
                    fingerJoint(
                    sideLength = (index % 2 == 0)?L:l,
                    jointSize = 20,
                    thickness = thickness,
                    in = fingerJointSide[index],
                    removeOverlapEnd = (fingerJointSide[index] == - 1 && fingerJointSide[(4 + index - 1) % 4] == 1),
                    removeOverlapBegin = (fingerJointSide[index] == - 1 && fingerJointSide[(index + 1) % 4] == 1)

                    );
                }
        }
    }


    translate([
                (- abs(fingerJointSide[3]) + abs(fingerJointSide[1])) * thickness / 2,
                (- abs(fingerJointSide[0]) + abs(fingerJointSide[2])) * thickness / 2,
        0])
        cube([L - (abs(fingerJointSide[1]) + abs(fingerJointSide[3])) * thickness,
                l - (abs(fingerJointSide[0]) + abs(fingerJointSide[2])) * thickness,
            thickness], true);

    joint(0);
    joint(1);
    joint(2);
    joint(3);
}


module fingerJoint(
sideLength = 100,
jointSize = 20,
in = 1,
thickness = 3,
removeOverlapBegin = false,
removeOverlapEnd = false) {
    numberOfJoints = floor(sideLength / (jointSize * 2));
    firstJointLength = (sideLength - ((numberOfJoints + (numberOfJoints - 1))) * jointSize) / 2.0;

    translate([- sideLength / 2, 0, - thickness / 2]) {
        if (in == 1) {
            for (i = [0: numberOfJoints - 1]) {
                translate([firstJointLength + (i * jointSize * 2), 0, 0]) {
                    cube([jointSize, thickness, thickness], false);
                }
            }
        }
        if (in == - 1) {
            translate([(removeOverlapBegin?thickness:0), 0, 0])
                cube([firstJointLength - (removeOverlapBegin? thickness: 0), thickness, thickness], false);
            if (numberOfJoints > 1) {
                for (i = [0: numberOfJoints - 2]) {
                    translate([firstJointLength + jointSize * (1 + i * 2), 0, 0]) {
                        cube([jointSize, thickness, thickness], false);
                    }
                }
            }
            translate([firstJointLength + (2 * numberOfJoints - 1) * jointSize, 0, 0])
                cube([firstJointLength - (removeOverlapEnd? thickness: 0), thickness, thickness], false);
        }
    }
}
