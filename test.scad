include <boxDefaultParams.scad>
include <boxBottom.scad>
include <boxSidePanel.scad>
include <boxFacingPanel.scad>
include <boxPanel.scad>
include <box.scad>
include <notches.scad>

boxHeight = 60;
boxWidth = 86;
boxLength = 420;

module boxSidePanelWithNotches() {
    difference() {
        boxSidePanel();
        notches(
        notchesWidth = 3.1,
        notchesDepth = 10,
        sideLength = boxLength,
        numberOfNotches = 10,
        isFace = false,
        panelHeight = boxHeight,
        thickness = boxThickness);
    }
}

separatorHeight = boxHeight - 10;
notchesHeight = 10;
/*
translate([250, 0, 0]) {
    color("blue") moveBoxBottom() boxBottom();
    color("purple", 0.2) moveBoxSidePanel(1) boxSidePanelWithNotches();
    color("red") moveBoxSidePanel(- 1) boxSidePanelWithNotches();
    color("green") moveBoxFacingPanel(1) boxFacingPanel();
    color("orange") moveBoxFacingPanel(- 1) boxFacingPanel();
    rotate(90, [1, 0, 0]) {
        translate([0, (boxHeight - separatorHeight) / 2, 0])
            cube([boxWidth - 2 * boxThickness - 0.1, separatorHeight, boxThickness], true);
        translate([0, (boxHeight - notchesHeight) / 2, 0])
            cube([boxWidth, notchesHeight, boxThickness], true);


    }
}



projection(cut = true)
    {
        color("blue") moveBoxBottomSheetCut() boxBottom();
        color("purple") moveBoxSidePanelSheetCut(1) boxSidePanelWithNotches();
        color("red") moveBoxSidePanelSheetCut(- 1) boxSidePanelWithNotches();
        color("green") moveBoxFacingPanelSheetCut(1) boxFacingPanel();
        color("orange") moveBoxFacingPanelSheetCut(- 1) boxFacingPanel();
    }
*/

projection(cut = true)
    {
        translate([0, (boxHeight - separatorHeight) / 2, 0])
            cube([boxWidth - 2 * boxThickness - 0.5, separatorHeight, boxThickness], true);
        translate([0, (boxHeight - notchesHeight) / 2, 0])
            cube([boxWidth, notchesHeight, boxThickness], true);
    }
